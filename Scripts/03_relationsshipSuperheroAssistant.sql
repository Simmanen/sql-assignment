ALTER TABLE Assistant
Add SuperHeroId INT NOT NULL;

ALTER TABLE Assistant
ADD CONSTRAINT FK_Assistant_Superhero
FOREIGN KEY (SuperHeroId)
REFERENCES SuperHero(Id);

