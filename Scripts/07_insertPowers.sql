USE SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES
('Oil Splash', 'Throws an oil splash on people'),
('Swedish button', 'Ability to reset stuff'),
('LaserBurger', 'Shoots a beam of burgers out of its mouth'),
('Brown Cheese', 'Just brown cheese because its nice');

INSERT INTO SuperheroPower (SuperHeroId, PowerId)
VALUES
(1, 1),
(1, 4),
(2, 2),
(3, 3), 
(3, 2); 