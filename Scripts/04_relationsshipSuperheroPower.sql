CREATE TABLE SuperheroPower (
	SuperHeroId INT NOT NULL,
	PowerId INT NOT NULL,
	CONSTRAINT PK_SuperheroPower PRIMARY KEY(SuperHeroId, PowerId),
	CONSTRAINT FK_SuperheroPower_Superhero FOREIGN KEY (SuperHeroId) REFERENCES Superhero(Id),
	CONSTRAINT FK_SuperheroPower_Power FOREIGN KEY (PowerId) REFERENCES Power(Id)
	);
