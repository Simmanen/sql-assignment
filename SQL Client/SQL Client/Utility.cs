﻿using Microsoft.Data.SqlClient;

namespace SQL_Client
{
    internal class Utility
    {
        /// <summary>
        /// Gets a SQL Server connection string for a local SQL Server Express instance with integrated security and trust server certificate options set to true.
        /// </summary>
        /// <returns>The SQL Server connection string.</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "N-NO-01-01-9791\\SQLEXPRESS01"; //Change with your own local
            builder.IntegratedSecurity = true;
            builder.InitialCatalog = "Chinook";
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }
    }
}
