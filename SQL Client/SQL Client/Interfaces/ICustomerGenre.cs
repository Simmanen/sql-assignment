﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Interfaces
{
    internal interface ICustomerGenre
    {
        void GetMostPopularGenre(int customerId);
    }
}
