﻿using SQL_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Interfaces
{
    internal interface ICustomer<T>
    {
        void AddCustomer(Customer customer);
        void UpdateCustomer(Customer customer);

        Customer GetCustomerById(int customerId);

        Customer GetCustomerByName(string name);

        List<Customer> GetCustomersByPage(int limit, int offset);

    }
}
