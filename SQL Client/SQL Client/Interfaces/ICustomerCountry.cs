﻿using SQL_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Interfaces
{
    internal interface ICustomerCountry
    {
        List<CustomerCountry> GetCustomersByCountry();
    }
}
