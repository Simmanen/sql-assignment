﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Models
{
    public record CustomerCountry(string Country, int TotalCustomers)
    {
        public override string ToString()
        {
            return $"{Country}: {TotalCustomers}";
        }
    }
}
