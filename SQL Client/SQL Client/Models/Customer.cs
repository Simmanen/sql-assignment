﻿

namespace SQL_Client.Models
{
    /// <summary>
    /// Represents a customer in the Chinook database.
    /// </summary>
    public record Customer(int Id, string? FirstName = null, string? LastName = "", string? Country = "", string? PostalCode = "", string? Phone = "", string? Email = "")
    {
        /// <summary>
        /// Returns a string that represents the current customer.
        /// </summary>
        /// <returns>A string that represents the current customer.</returns>
        public override string? ToString()
        {
            return $"{Id},{FirstName},{LastName}, {Country}, {PostalCode}, {Phone}, {Email}";
        }
    }
}
