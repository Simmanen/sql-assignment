﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Models
{
    public record CustomerGenre(int CustomerId, string FirstName, string LastName, string MostPopularGenre);

}
