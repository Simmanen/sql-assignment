﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Client.Models
{
    public record CustomerSpender(int CustomerId, string FirstName, string LastName, decimal TotalSpent);
}
