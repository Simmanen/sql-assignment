﻿using Microsoft.Data.SqlClient;
using SQL_Client.Interfaces;
using SQL_Client.Models;

namespace SQL_Client.Repositories
{
    /// <summary>
    /// Represents a repository for retrieving information about customers who have spent the most amount of money.
    /// </summary>
    /// <remarks>
    /// This repository retrieves data from the Customer and Invoice tables in the Chinook database using SQL queries.
    /// </remarks>
    internal class CustomerSpenderRepository : ICustomerSpender
    {
        /// <summary>
        /// Retrieves a list of customers who have spent the most amount of money.
        /// </summary>
        /// <returns>A list of CustomerSpender objects.</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customers = new List<CustomerSpender>();
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "SELECT c.CustomerId, c.FirstName, c.LastName, SUM(i.Total) as TotalSpent " +
                             "FROM Customer c " +
                             "INNER JOIN Invoice i ON c.CustomerId = i.CustomerId " +
                             "GROUP BY c.CustomerId, c.FirstName, c.LastName " +
                             "ORDER BY TotalSpent DESC";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customers.Add(new CustomerSpender(
                                reader.GetInt32(0),
                                reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                reader.GetDecimal(3)));
                        }
                    }
                }
            }

            return customers;
        }
    }
}
