﻿using Microsoft.Data.SqlClient;
using SQL_Client.Interfaces;
using SQL_Client.Models;

namespace SQL_Client.Repositories
{
    /// <summary>
    /// A repository for accessing customer country data in the Chinook database.
    /// </summary>
    internal class CustomerCountryRepository : ICustomerCountry
    {
        /// <summary>
        /// Gets the number of customers per country from the Chinook database.
        /// </summary>
        /// <returns>A list of <see cref="CustomerCountry"/> objects representing the number of customers per country.</returns>
        public List<CustomerCountry> GetCustomersByCountry()
        {
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();

                string sql = "SELECT Country, COUNT(*) AS TotalCustomers " +
                             "FROM Customer GROUP BY Country ORDER BY TotalCustomers DESC";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    List<CustomerCountry> customersByCountry = new List<CustomerCountry>();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string country = reader.GetString(0);
                            int totalCustomers = reader.GetInt32(1);

                            customersByCountry.Add(new CustomerCountry(country, totalCustomers));
                        }
                    }
                    return customersByCountry;
                }
            }
        }
    }
}
