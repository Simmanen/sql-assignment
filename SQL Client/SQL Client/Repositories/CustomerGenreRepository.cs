﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using SQL_Client.Interfaces;

namespace SQL_Client.Repositories
{
    internal class CustomerGenreRepository : ICustomerGenre
    {
        public void GetMostPopularGenre(int customerId)
        {
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = @"
                    SELECT g.Name AS PopularGenre
                    FROM Customer c
                    INNER JOIN Invoice i ON c.CustomerId = i.CustomerId
                    INNER JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId
                    INNER JOIN Track t ON il.TrackId = t.TrackId
                    INNER JOIN Genre g ON t.GenreId = g.GenreId
                    WHERE c.CustomerId = @Id
                    GROUP BY g.GenreId, g.Name
                    HAVING COUNT(*) = (
                        SELECT MAX(track_count)
                        FROM (
                            SELECT COUNT(*) AS track_count
                            FROM Customer c
                            INNER JOIN Invoice i ON c.CustomerId = i.CustomerId
                            INNER JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId
                            INNER JOIN Track t ON il.TrackId = t.TrackId
                            INNER JOIN Genre g ON t.GenreId = g.GenreId
                            WHERE c.CustomerId = @Id
                            GROUP BY g.GenreId
                            ) AS genre_track_counts
                            )";



                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@Id", customerId);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string popularGenre = reader.GetString(0);
                            Console.WriteLine($"Popular genre for customer {customerId}: {popularGenre}");
                        }
                    }
                }
            }
        }
    }
}
