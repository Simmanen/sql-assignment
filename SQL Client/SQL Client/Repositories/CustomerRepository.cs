﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using SQL_Client.Interfaces;
using SQL_Client.Models;

namespace SQL_Client.Repositories
{    
    /// <summary>
    /// Represents a repository of customers that interacts with a SQL Server database.
    /// </summary>
    public class CustomerRepository : ICustomer<Customer>
    {   
        /// <summary>
        /// Gets all customers from the database.
        /// </summary>
        /// <returns>A list of all customers in the database.</returns>
        public List<Customer> GetAll()
        {
            using var builder = new SqlConnection(Utility.GetConnectionString());

            List<Customer> customers = new List<Customer>();

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customers.Add(new Customer(
                                reader.GetInt32(0),
                                reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                                reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                                reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                                reader.IsDBNull(6) ? string.Empty : reader.GetString(6)));
                        }
                    }
                    return customers;
                }
            }
        }

        /// <summary>
        /// Gets a customer from the database by ID.
        /// </summary>
        /// <param name="customerId">The ID of the customer to retrieve.</param>
        /// <returns>The customer with the specified ID.</returns>
        /// <exception cref="Exceptions.CustomerNotFoundException">Thrown if customer with specified ID is not found.</exception>
        public Customer GetCustomerById(int customerId)
        {
            Customer customer = null;
            using var builder = new SqlConnection(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                    " WHERE CustomerId = @customerId";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@customerId", customerId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customer = (new Customer(
                                reader.GetInt32(0),
                                reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                                reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                                reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                                reader.IsDBNull(6) ? string.Empty : reader.GetString(6)));
                        }
                    }
                    if (customer == null)
                    {
                        throw new Exceptions.CustomerNotFoundException("Customer with ID " + customerId + " not found.");
                    }

                    return customer;
                }
            }
        }

        /// <summary>
        /// Gets a customer from the database by name.
        /// </summary>
        /// <param name="name">The name of the customer to retrieve.</param>
        /// <returns>The customer with the specified name.</returns>
        /// <exception cref="Exceptions.CustomerNotFoundException">Thrown if customer with specified name is not found.</exception>
        public Customer GetCustomerByName(string name)
        {
            Customer customer = null;
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                    " WHERE FirstName = @name";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@name", name);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customer = (new Customer(
                                reader.GetInt32(0),
                                reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                                reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                                reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                                reader.IsDBNull(6) ? string.Empty : reader.GetString(6)));
                        }
                    }
                }
            }
            if (customer == null)
            {
                throw new Exceptions.CustomerNotFoundException("Customer with name " + name + " not found.");
            }

            return customer;
        }

        /// <summary>
        /// Gets a list of customers from the database with pagination.
        /// </summary>
        /// <param name="limit">The maximum number of customers to retrieve.</param>
        /// <param name="offset">The number of customers to skip before beginning to retrieve.</param>
        /// <returns>A list of customers with the specified pagination criteria.</returns>
        public List<Customer> GetCustomersByPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                    " ORDER BY FirstName OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@offset", offset);
                    cmd.Parameters.AddWithValue("@limit", limit);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            customers.Add(new Customer(
                                reader.GetInt32(0),
                                reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                                reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                                reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                                reader.IsDBNull(6) ? string.Empty : reader.GetString(6)));
                        }
                    }
                }
            }

            return customers;
        }

        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="customer">The customer to add.</param>
        public void AddCustomer(Customer customer)
        {
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");
                string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                             "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                    cmd.Parameters.AddWithValue("@Country", customer.Country);
                    cmd.Parameters.AddWithValue("PostalCode", customer.PostalCode);
                    cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                    cmd.Parameters.AddWithValue("@Email", customer.Email);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Updates an existing customer in the database.
        /// </summary>
        /// <param name="customer">The customer to update.</param>
        public void UpdateCustomer(Customer customer)
        {
            var builder = new SqlConnectionStringBuilder(Utility.GetConnectionString());

            using (SqlConnection con = new SqlConnection(builder.ConnectionString))
            {
                con.Open();
                Console.WriteLine("Connected");

                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE Customer SET ");

                List<string> fieldsToUpdate = new List<string>();
                if (!string.IsNullOrEmpty(customer.FirstName))
                {
                    fieldsToUpdate.Add($"FirstName = @FirstName");
                }
                if (!string.IsNullOrEmpty(customer.LastName))
                {
                    fieldsToUpdate.Add($"LastName = @LastName");
                }
                if (!string.IsNullOrEmpty(customer.Country))
                {
                    fieldsToUpdate.Add($"Country = @Country");
                }
                if (!string.IsNullOrEmpty(customer.PostalCode))
                {
                    fieldsToUpdate.Add($"PostalCode = @PostalCode");
                }
                if (!string.IsNullOrEmpty(customer.Phone))
                {
                    fieldsToUpdate.Add($"Phone = @Phone");
                }
                if (!string.IsNullOrEmpty(customer.Email))
                {
                    fieldsToUpdate.Add($"Email = @Email");
                }

                if (fieldsToUpdate.Count == 0)
                {
                    throw new ArgumentException("At least one field to update must be specified.");
                }

                sb.Append(string.Join(", ", fieldsToUpdate));
                sb.Append(" WHERE CustomerId = @CustomerId");

                using (SqlCommand cmd = new SqlCommand(sb.ToString(), con))
                {
                    cmd.Parameters.AddWithValue("@CustomerId", customer.Id);
                    if (!string.IsNullOrEmpty(customer.FirstName))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    }
                    if (!string.IsNullOrEmpty(customer.LastName))
                    {
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                    }
                    if (!string.IsNullOrEmpty(customer.Country))
                    {
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                    }
                    if (!string.IsNullOrEmpty(customer.PostalCode))
                    {
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                    }
                    if (!string.IsNullOrEmpty(customer.Phone))
                    {
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                    }
                    if (!string.IsNullOrEmpty(customer.Email))
                    {
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                    }

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
