﻿using SQL_Client.Models;
using SQL_Client.Repositories;

new CustomerRepository().GetAll().ForEach(Console.WriteLine);
Console.WriteLine(new CustomerRepository().GetCustomerById(5).ToString());
Console.WriteLine(new CustomerRepository().GetCustomerByName("Julia").ToString());
new CustomerRepository().GetCustomersByPage(10, 20).ForEach(c => Console.WriteLine(c.ToString()));

//Functionality to add customers
//var customerRepository = new CustomerRepository();
//customerRepository.AddCustomer(new Customer(0, "Nicholas", "Big Token Spender", "Norway", "1337", "123-45-678", "WoWAddict@mage.com"));

new CustomerCountryRepository().GetCustomersByCountry().ForEach(cc => Console.WriteLine($"{cc.Country}: {cc.TotalCustomers}"));

Console.WriteLine(string.Join(Environment.NewLine, new CustomerSpenderRepository().GetHighestSpenders()
    .Select(c => $"{c.FirstName} {c.LastName} - Total Spent: {c.TotalSpent:C}")));

Customer oldCustomer = new CustomerRepository().GetCustomerById(1);

Customer customerToUpdate = new Customer(oldCustomer.Id)
{
    FirstName="Arg",
    Email = "argdude1409@hotmail.co.uk"
};

new CustomerRepository().UpdateCustomer(customerToUpdate);
new CustomerGenreRepository().GetMostPopularGenre(12);
