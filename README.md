# SQL Assignment

This project involves the creation of SQL scripts and a console application in C# that interacts with the database. The aim of this project is to meet the minimum requirements while allowing for expansion of functionality.
## Dependencies:

SQL Server
Visual Studio or other C# development environment

## Installation

To install and run this project, follow these steps:

Install the .NET SDK on your machine if you haven't already.
Clone this repository to your local machine using git clone https://gitlab.com/Simmanen/sql-assignment
### SQL Scripts

Install SQL Server on your system if not already installed.
Run the SQL scripts to create the database and its assoociated tables.

### SQL Client Visual Studio

Navigate to the project directory using cd your-repo.
Change the sql connection string in Utility to your own.
Build the project using the command dotnet build.
Run the project using the command dotnet run.

## Usage

Everything is run trough Program.cs
If you want to test the functions to add a customer or update an existing one the code is commentet out in the Program.cs

## Contributers

Simen Heiestad Mandt
Joachim Noor Atamna
